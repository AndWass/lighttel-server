defmodule LighttelServer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    authenticator_backend = Application.get_env(:lighttel_server, :authenticator)

    publish_backend =
      Application.get_env(
        :lighttel_server,
        :publish_backend,
        LighttelServer.PubSub.PublishInspectSink
      )

    children = [
      # Starts a worker by calling: LighttelServer.Worker.start_link(arg)
      # {LighttelServer.Worker, arg},
      LighttelServer.Authenticator.child_spec(
        authenticator_backend,
        name: LighttelServer.Authenticator
      ),
      LighttelServer.PubSub.PublishSink.child_spec(
        publish_backend,
        name: LighttelServer.PubSub.PublishSink
      ),
      {DynamicSupervisor, strategy: :one_for_one, name: LighttelServer.ClientSupervisor},
      Supervisor.child_spec(
        {Task,
         fn ->
           Process.register(self(), LighttelServer.TcpAcceptor)
           LighttelServer.Tcp.accept(9321)
         end},
        restart: :permanent
      )
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: LighttelServer.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
