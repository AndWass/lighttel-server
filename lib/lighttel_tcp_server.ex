require Logger

defmodule LighttelServer.Tcp do
  def accept(port) do
    listen_opts = [:binary, active: true, packet: :raw, send_timeout: 60, reuseaddr: true]
    {:ok, socket} = :gen_tcp.listen(port, listen_opts)
    {:ok, actual_port} = :inet.port(socket)
    Logger.info("Accepting sockets on port #{actual_port}")
    loop_acceptor(socket)
  end

  defp loop_acceptor(socket) do
    {:ok, client} = :gen_tcp.accept(socket)
    on_new_client(client)
    loop_acceptor(socket)
  end

  defp on_new_client(client_socket) do
    client_spec = LighttelServer.TcpClient.child_spec(socket: client_socket)
    {:ok, child} = DynamicSupervisor.start_child(LighttelServer.ClientSupervisor, client_spec)
    :gen_tcp.controlling_process(client_socket, child)
  end
end
