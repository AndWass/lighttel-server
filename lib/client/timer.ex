defmodule LighttelServer.Client.Timer do
  use GenServer

  @start_state %{}

  @doc """
  Starts or restarts a timer that will call func/1 after it has timed out.
  Type can be either `:singleshot` or `:recurring`.
  """
  def start_timer(timer_handler, type, identifier, timeout, func) when is_atom(type) do
    GenServer.cast(timer_handler, {:start, type, identifier, timeout, func})
    :started
  end

  def restart_timer(timer_handler, identifier) do
    GenServer.cast(timer_handler, {:restart, identifier})
    :ok
  end

  def stop_timer(timer_handler, identifier) do
    GenServer.cast(timer_handler, {:stop, identifier})
    :ok
  end

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, self(), opts)
  end

  ## Private functions

  defp new_timer(timers, type = :singleshot, identifier, timeout, func) do
    {:ok, timer} = :timer.send_after(trunc(timeout * 1000), {:timeout, type, identifier, func})
    Map.put(timers, identifier, make_state_entry(timer, type, timeout, func))
  end

  defp new_timer(timers, type = :recurring, identifier, timeout, func) do
    {:ok, timer} = :timer.send_interval(trunc(timeout * 1000), {:timeout, type, identifier, func})
    Map.put(timers, identifier, make_state_entry(timer, type, timeout, func))
  end

  defp reload_timer(timers, identifier) do
    existing_timer = timers[identifier]

    if existing_timer !== nil do
      {_existing_timer, type, timeout, func} = existing_timer
      timers = remove_timer(timers, identifier)
      {:ok, new_timer(timers, type, identifier, timeout, func)}
    else
      {:notfound, timers}
    end
  end

  defp remove_timer(timers, identifier) do
    existing_timer = timers[identifier]

    if existing_timer !== nil do
      {existing_timer, _type, _timeout, _func} = existing_timer
      {:ok, _} = :timer.cancel(existing_timer)
    end

    Map.delete(timers, identifier)
  end

  defp make_state_entry(timer, type, timeout, func) do
    {timer, type, timeout, func}
  end

  ## GenServer callbacks

  def init(parent) do
    Process.monitor(parent)
    start_state = @start_state
    {:ok, start_state}
  end

  def handle_cast({:start, type, identifier, timeout, func}, timers) do
    timers = remove_timer(timers, identifier)
    timers = new_timer(timers, type, identifier, timeout, func)
    {:noreply, timers}
  end

  def handle_cast({:restart, identifier}, timers) do
    {_, timers} = reload_timer(timers, identifier)
    {:noreply, timers}
  end

  def handle_cast({:stop, identifier}, _from, timers) do
    timers = remove_timer(timers, identifier)
    {:noreply, timers}
  end

  def handle_info({:timeout, :singleshot, identifier, func}, timers) do
    timers = remove_timer(timers, identifier)
    func.(identifier)
    {:noreply, timers}
  end

  def handle_info({:timeout, :recurring, identifier, func}, timers) do
    func.(identifier)
    {:noreply, timers}
  end
end
