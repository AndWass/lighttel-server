defmodule LighttelServer.Client.ClientHandler do
  use GenServer, restart: :temporary

  alias Lighttel.Packet.Connect
  alias Lighttel.Packet.PutPublishHandle
  alias Lighttel.Packet.PutAuth
  alias Lighttel.Packet.Publish
  alias Lighttel.Packet.BatchPublish
  alias Lighttel.Packet.Ping
  alias LighttelServer.Authenticator
  alias LighttelServer.PubSub.PublishSink

  @initial_state %{
    :remote_compression_support => [],
    :remote_batch_publish_support => false,
    :authentications => nil,
    :auth_timeout_timers => %{},
    :publish_handles => %{}
  }

  ## Module methods

  def start_link(_sender, opts) do
    GenServer.start_link(__MODULE__, [], opts)
  end

  def handle_packet(server, packet) do
    GenServer.call(server, packet)
  end

  ## GenServer callbacks

  def init(_args) do
    {:ok, %{@initial_state | authentications: Authenticator.new_server_state(Authenticator)}}
  end

  def handle_call(
        %Connect{
          compression_support: remote_compression_support,
          batch_publish: remote_batch_publish_support
        },
        _from,
        state
      ) do
    response =
      %Connect.Ack{}
      |> Map.put(:compression_support, Lighttel.Compression.algorithms())

    result = make_reply(response)

    newstate =
      state
      |> Map.put(:remote_compression_support, remote_compression_support)
      |> Map.put(:remote_batch_publish_support, remote_batch_publish_support)

    {:reply, result, newstate}
  end

  def handle_call(
        %PutAuth{
          handle: handle
        } = auth,
        _from,
        state
      ) do
    {reply, new_auths, new_auth_timers} =
      case Authenticator.authenticate(Authenticator, state.authentications, auth) do
        {:ok, valid_for, new_auths} ->
          reply =
            make_reply(%Lighttel.Packet.PutAuth.Ack{
              valid_for: valid_for,
              handle: handle
            })

          new_auth_timers = restart_auth_timer(state.auth_timeout_timers, handle, valid_for)
          {reply, new_auths, new_auth_timers}

        {:nack, reason, new_auths} ->
          reply =
            make_reply(%Lighttel.Packet.PutAuth.Nack{
              handle: handle,
              reason: reason
            })

          {reply, new_auths, state.auth_timeout_timers}
      end

    state = %{state | authentications: new_auths, auth_timeout_timers: new_auth_timers}

    {:reply, reply, state}
  end

  def handle_call(
        %PutPublishHandle{
          handles: handles
        },
        _from,
        state
      ) do
    {all_ids, empty_keys, nonempty_handles} =
      handles
      |> Enum.reduce({[], [], %{}}, fn {k, v}, {a, e, ne} ->
        new_a = a ++ [k]
        new_e = e ++ if byte_size(v) === 0, do: [k], else: []
        new_ne = if byte_size(v) !== 0, do: Map.put(ne, k, v), else: ne
        {new_a, new_e, new_ne}
      end)

    all_success =
      all_ids
      |> Enum.map(&{&1, 0})
      |> Map.new()

    ack = %PutPublishHandle.Ack{
      results: all_success
    }

    new_handles =
      state.publish_handles
      |> Map.drop(empty_keys)
      |> Map.merge(nonempty_handles, fn _k, _v1, v2 -> v2 end)

    new_state = %{state | publish_handles: new_handles}

    {:reply, make_reply(ack), new_state}
  end

  def handle_call(%Publish{} = msg, _from, state) do
    reply =
      case handle_single_publish(msg, state) do
        {:ack, msg_id} -> make_reply(%Publish.Ack{message_ids: [msg_id]})
        :nack -> :ok
      end

    {:reply, reply, state}
  end

  def handle_call(%BatchPublish{messages: messages}, _from, state) do
    ack_ids =
      messages
      |> Enum.map(fn msg ->
        case handle_single_publish(msg, state) do
          {:ack, msg_id} -> msg_id
          :nack -> []
        end
      end)
      |> List.flatten()

    reply =
      if Enum.empty?(ack_ids) do
        :ok
      else
        make_reply(%Publish.Ack{message_ids: ack_ids})
      end

    {:reply, reply, state}
  end

  def handle_call(%Ping{}, _from, state) do
    {:reply, :ok, state}
  end

  def handle_info(
        {:auth_timeout, handle},
        %{authentications: auths, auth_timeout_timers: timers} = state
      ) do
    new_auths = Authenticator.remove_authentication(Authenticator, auths, handle)

    {:noreply,
     %{state | authentications: new_auths, auth_timeout_timers: Map.delete(timers, handle)}}
  end

  defp handle_single_publish(%Publish{} = msg, state) do
    msg = map_endpoint(msg, state.publish_handles)

    if Authenticator.publish_allowed?(Authenticator, state.authentications, msg.endpoint) and
         PublishSink.publish(PublishSink, msg) === :ok do
      {:ack, msg.message_id}
    else
      :nack
    end
  end

  defp map_endpoint(
         %Publish{
           endpoint: ep
         } = msg,
         endpoint_map
       )
       when is_integer(ep) do
    %Publish{msg | endpoint: endpoint_map[ep]}
  end

  defp map_endpoint(msg, _) do
    msg
  end

  defp restart_auth_timer(auth_timers, handle, timeout) do
    existing_timer = auth_timers[handle]

    if existing_timer !== nil do
      :timer.cancel(existing_timer)
    end

    new_auth_timers =
      if timeout !== 0 do
        {:ok, ref} = :timer.send_after(timeout * 1000, self(), {:auth_timeout, handle})
        Map.put(auth_timers, handle, ref)
      else
        Map.delete(auth_timers, handle)
      end

    new_auth_timers
  end

  defp make_reply(packets) when is_list(packets) do
    {:reply, packets}
  end

  defp make_reply(packet) when is_map(packet) do
    {:reply, [packet]}
  end
end
