defmodule LighttelServer.PubSub.PublishInspectSink do
  @behaviour LighttelServer.PubSub.PublishSink

  def start_link(_) do
    {:ok, nil}
  end

  def publish(nil, packet) do
    IO.inspect("******* Publish *******")
    IO.inspect(packet)
    IO.inspect("***** Publish end *****")
    :ok
  end
end
