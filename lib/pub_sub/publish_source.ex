defmodule LighttelServer.PubSub.PublishSource do
  use GenServer

  @moduledoc """
  A `PublishSource` is a source where clients can subscribe to endpoints.
  Each message must be associated with a unique identifier, that can be used to identify the message
  regardless of client and endpoint.

  Messages must be delivered to clients in the form of messages where the message consists of the following following:
  `{:publish, [{msg_uid, endpoint, payload}]}`

  When a message has been sent and successfully ACKed the ack function must be called
  with the corresponding msg_uid. Note that msg_uid re

  """

  @type msg_uid :: term
  @type message :: {msg_uid, String.t(), binary}

  @callback start_link(term) :: {:ok, term}
  @callback subscribe(term, String.t() | [String.t()], [term]) :: :ok
  @callback unsubscribe(term, String.t()) :: :ok
  @callback ack(term, msg_uid) :: :ok

  defstruct backend: nil, handle: nil, ep_pid: %{}, monitor_refs: %{}

  alias __MODULE__

  def start_link(backend \\ LighttelServer.PubSub.SimpleSource, opts) do
    {:ok, handle} = backend.start_link(opts)
    GenServer.start_link(__MODULE__, %PublishSource{backend: backend, handle: handle}, opts)
  end

  @impl true
  def init(state) do
    {:ok, state}
  end

  @impl true
  def handle_call({:subscribe, eps, opts}, {pid, _tag}, state) do
    new_state = maybe_add_ep_pid(state, eps, pid)

    response = state.backend.subscribe(state.handle, eps, opts)

    {:reply, response, new_state}
  end

  @impl true
  def handle_call({:unsubscribe, ep}, {pid, _tag}, state) do
    state.backend.unsubscribe(state.handle, ep)

    next_ep_pid_entry =
      state.ep_pid
      |> Map.get(ep, MapSet.new())
      |> MapSet.delete(pid)

    next_ep_pid = Map.put(state.ep_pid, ep, next_ep_pid_entry)
    {:reply, :ok, %PublishSource{state | ep_pid: next_ep_pid}}
  end

  @impl true
  def handle_call({:ack, msg_uid}, _from, state) do
    response = state.backend.ack(state.handle, msg_uid)
    {:reply, response, state}
  end

  @impl true
  def handle_info({:publish, messages}, state) do
    messages
    |> Enum.group_by(fn {_msg_uid, endpoint, _payload} ->
      pids_from_endpoint(state.ep_pid, endpoint)
    end)
    |> send_messages()

    {:noreply, state}
  end

  @impl true
  def handle_info({:DOWN, _ref, :process, pid, _reason}, state) do
    new_ep_pid = for {ep, pids} <- state.ep_pid, into: %{}, do: {ep, MapSet.delete(pids, pid)}
    new_monitor_ref = Map.delete(state.monitor_refs, pid)

    {:noreply, %PublishSource{state | ep_pid: new_ep_pid, monitor_refs: new_monitor_ref}}
  end

  ## Public API
  def subscribe(server, ep, opts) do
    ep_list = if is_list(ep), do: ep, else: [ep]
    GenServer.call(server, {:subscribe, ep_list, opts})
  end

  def ack(server, msg_uid) do
    GenServer.call(server, {:ack, msg_uid})
  end

  ## Private functions

  defp maybe_add_ep_pid(state, eps, pid) when is_list(eps) do
    eps
    |> Enum.reduce(state, fn ep, acc ->
      new_ep_pid_entry =
        Map.get(acc.ep_pid, ep, MapSet.new())
        |> MapSet.put(pid)

      new_ep_pid = Map.put(acc.ep_pid, ep, new_ep_pid_entry)

      next_monitor_refs =
        case acc.monitor_refs[pid] do
          nil -> Map.put(acc.monitor_refs, pid, Process.monitor(pid))
          _ -> acc.monitor_refs
        end

      %PublishSource{acc | ep_pid: new_ep_pid, monitor_refs: next_monitor_refs}
    end)
  end

  defp send_messages(pid_messages) do
    pid_messages
    |> Enum.each(fn {pids, messages} ->
      for pid <- pids, do: send(pid, {:publish, messages})
    end)

    :ok
  end

  defp pids_from_endpoint(pid_lookup, endpoint) do
    case pid_lookup[endpoint] do
      nil -> MapSet.new()
      mapset -> mapset
    end
  end
end
